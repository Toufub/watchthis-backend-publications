#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["PublicationsMicroService/PublicationsMicroService.csproj", "./MicroService/PublicationsMicroService.csproj"]
COPY ["PublicationsMicroServiceTests/PublicationsMicroServiceTests.csproj", "./Tests/PublicationsMicroServiceTests.csproj"]
RUN dotnet restore "./MicroService/PublicationsMicroService.csproj"
RUN dotnet restore "./Tests/PublicationsMicroServiceTests.csproj"
COPY ./PublicationsMicroService ./MicroService/
COPY ./PublicationsMicroServiceTests ./Tests/
RUN dotnet build "MicroService/PublicationsMicroService.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "MicroService/PublicationsMicroService.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "PublicationsMicroService.dll"]
