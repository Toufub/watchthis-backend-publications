﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text.Json.Nodes;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PublicationsMicroService.Models.Dto;
using PublicationsMicroService.Models.EntityFramework;
using PublicationsMicroService.Models.Repository;
using PublicationsMicroService.Services;
using UtilisateursMicroService.Services;

namespace PublicationsMicroService.Controllers
{
    [Route("")]
    [ApiController]
    public class PublicationsController : ControllerBase
    {
        private readonly IEntityRepository<Publication> dataRepository;
        private IConfiguration _config;

        public PublicationsController(IEntityRepository<Publication> dataRepo, IConfiguration config)
        {
            dataRepository = dataRepo;
            _config = config;
        }

        [HttpGet]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<Publication>>> GetPublications()
        {
            return await dataRepository.GetAll();
        }

        [HttpGet("[action]/{id}")]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<Publication>>> ByUtilisateur(int id)
        {
            return await dataRepository.GetByUtilisateurId(id);
        }

        [HttpGet("[action]/{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<Publication>>> ByFilm(string id)
        {
            return await dataRepository.GetByFilmId(id);
        }

        [HttpGet("[action]/{filmId}/{utilisateurId}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<Publication>> ByFilmUtilisateur(string filmId, int utilisateurId)
        {
            var publication = await dataRepository.GetByFilmUtilisateurId(filmId, utilisateurId);

            if (publication.Value == null)
            {
                return NotFound();
            }

            return publication;
        }

        [HttpPut("{filmId}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> PutPublication(string filmId, Publication publication)
        {
            Authenticator.init(_config);
            JwtSecurityToken token = Authenticator.GetToken(HttpContext.Request.Headers["Authorization"]);
            var tokenId = int.Parse(token.Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value);
            if (filmId != publication.FilmId || publication.UtilisateurId != tokenId)
            {
                return Unauthorized("Pas votre publication");
            }

            var publicationToUpdate = await dataRepository.GetByFilmUtilisateurId(filmId, tokenId);
            if (publicationToUpdate.Value == null)
            {
                return NotFound("publication non trouvée");
            }
            dataRepository.Update(publicationToUpdate.Value, publication);

            return NoContent();
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<Publication>> PostPublication(Publication publication)
        {
            Authenticator.init(_config);
            JwtSecurityToken token = Authenticator.GetToken(HttpContext.Request.Headers["Authorization"]);
            var tokenId = int.Parse(token.Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value);
            var publicationExistante = await dataRepository.GetByFilmUtilisateurId(publication.FilmId, tokenId);
            if(publicationExistante.Value == null)
            {
                publication.UtilisateurId = tokenId;
                await dataRepository.Add(publication);
                return CreatedAtAction("ByFilmUtilisateur", new
                {
                    filmId = publication.FilmId,
                    utilisateurId = publication.UtilisateurId
                }, publication);
            } else
            {
                return BadRequest("Existe déjà");
            }
        }

        [HttpPost("[action]")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<Publication>>> ByUtilisateurIds(ListUtilisateurId list)
        {
            List<Publication> publications = new List<Publication>();
            foreach (var id in list.ids)
            {
                var publicationsUtilisateur = await dataRepository.GetByUtilisateurId(id);
                foreach(Publication publication in publicationsUtilisateur.Value)
                {
                    publications.Add(publication);
                }
            }
            return publications;
        }

        [HttpDelete("{filmId}/{utilisateurId}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> DeletePublication(string filmId, int utilisateurId)
        { 
            Authenticator.init(_config);
            JwtSecurityToken token = Authenticator.GetToken(HttpContext.Request.Headers["Authorization"]);
            var tokenId = int.Parse(token.Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value);
            var publication = await dataRepository.GetByFilmUtilisateurId(filmId, utilisateurId);
            if(publication.Value.UtilisateurId != tokenId)
            {
                return Unauthorized("Ce n'est pas votre publication");
            }
            if (publication.Value == null)
            {
                return NotFound("publication non trouvée");
            }

            await dataRepository.Delete(publication.Value);

            return NoContent();
        }

    }
}
