﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PublicationsMicroService.Migrations
{
    /// <inheritdoc />
    public partial class createbddpublications : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "public");

            migrationBuilder.CreateTable(
                name: "t_p_publication_pbl",
                schema: "public",
                columns: table => new
                {
                    flmid = table.Column<string>(name: "flm_id", type: "character varying(15)", maxLength: 15, nullable: false),
                    utlid = table.Column<int>(name: "utl_id", type: "integer", nullable: false),
                    pblnote = table.Column<int>(name: "pbl_note", type: "integer", nullable: false),
                    pblcommentaire = table.Column<string>(name: "pbl_commentaire", type: "character varying(256)", maxLength: 256, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_publication", x => new { x.flmid, x.utlid });
                    table.CheckConstraint("ck_pbl_note", "pbl_note between 0 and 10");
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "t_p_publication_pbl",
                schema: "public");
        }
    }
}
