﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PublicationsMicroService.Migrations
{
    /// <inheritdoc />
    public partial class adddatepublication : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "pbl_datepublication",
                schema: "public",
                table: "t_p_publication_pbl",
                type: "Date",
                nullable: false,
                defaultValueSql: "now()");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "pbl_datepublication",
                schema: "public",
                table: "t_p_publication_pbl");
        }
    }
}
