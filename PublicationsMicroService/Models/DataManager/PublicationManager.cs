﻿using PublicationsMicroService.Models.EntityFramework;
using PublicationsMicroService.Models.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Text;

namespace PublicationsMicroService.Models.DataManager
{
    public class PublicationManager : IEntityRepository<Publication>
    {
        private readonly PublicationsDbContext publicationsDbContext;
        public PublicationManager() { }
        public PublicationManager(PublicationsDbContext context)
        {
            publicationsDbContext = context;
        }
        public async Task<ActionResult<IEnumerable<Publication>>> GetAll()
        {
            return await publicationsDbContext.Publications.ToListAsync();
        }
        public async Task<ActionResult<IEnumerable<Publication>>> GetByFilmId(string id)
        {
            return await publicationsDbContext.Publications
                .Where(p => p.FilmId == id)
                .ToListAsync();
        }
        public async Task<ActionResult<IEnumerable<Publication>>> GetByUtilisateurId(int id)
        {
            return await publicationsDbContext.Publications
                .Where(p => p.UtilisateurId == id)
                .ToListAsync();
        }
        public async Task<ActionResult<Publication>> GetByFilmUtilisateurId(string filmId, int utilisateurId)
        {
            return await publicationsDbContext.Publications
                .Where(p => p.FilmId == filmId)
                .Where(p => p.UtilisateurId == utilisateurId)
                .FirstOrDefaultAsync();
        }
        public async Task Add(Publication entity)
        {
            await publicationsDbContext.Publications.AddAsync(entity);
            await publicationsDbContext.SaveChangesAsync();
        }
        public async Task Update(Publication publication, Publication entity)
        {
            publicationsDbContext.Entry(publication).State = EntityState.Modified;
            publication.Commentaire = entity.Commentaire;
            publication.Note = entity.Note;
            await publicationsDbContext.SaveChangesAsync();
        }

        public async Task Delete(Publication publication)
        {
            publicationsDbContext.Publications.Remove(publication);
            await publicationsDbContext.SaveChangesAsync();
        }
    }
}
