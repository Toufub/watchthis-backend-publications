﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace PublicationsMicroService.Models.EntityFramework
{
    [Table("t_p_publication_pbl")]
    public class Publication
    {
        [Key]
        [Column("flm_id")]
        [StringLength(15)]
        public string FilmId { get; set; }

        [Key]
        [Column("utl_id")]
        public int UtilisateurId { get; set; }

        [Required]
        [Column("pbl_note")]
        public int Note { get; set; }

        [Column("pbl_commentaire")]
        [StringLength(256)]
        public string? Commentaire { get; set; }

        [Column("pbl_datepublication")]
        public DateTime DatePublication { get; set; }
    }
}
