﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using static PublicationsMicroService.Models.EntityFramework.Publication;
using PublicationsMicroService.Models.EntityFramework;
using Microsoft.CodeAnalysis;

namespace PublicationsMicroService.Models.EntityFramework;

public partial class PublicationsDbContext : DbContext

{
    public static readonly ILoggerFactory MyLoggerFactory = LoggerFactory.Create(builder => builder.AddConsole());

    public PublicationsDbContext()
    {
    }

    public PublicationsDbContext(DbContextOptions<PublicationsDbContext> options)
        : base(options)
    {
    }
    public virtual DbSet<Publication> Publications { get; set; } = null!;
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (!optionsBuilder.IsConfigured)
        {
            optionsBuilder.UseLoggerFactory(MyLoggerFactory).EnableSensitiveDataLogging();
        }
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.HasDefaultSchema("public");

        modelBuilder.Entity<Publication>(entity =>
        {
            entity.HasKey(e => new { e.FilmId, e.UtilisateurId })
            .HasName("pk_publication");

            entity.Property(e => e.DatePublication).HasColumnType("Date");
            entity.Property(e => e.DatePublication).HasDefaultValueSql("now()");

            entity.HasCheckConstraint("ck_pbl_note", "pbl_note between 0 and 10");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
