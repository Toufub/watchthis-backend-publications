﻿using Microsoft.AspNetCore.Mvc;

namespace PublicationsMicroService.Models.Repository
{
    public interface IEntityRepository<TEntity>
    {
        Task<ActionResult<IEnumerable<TEntity>>> GetAll();
        Task<ActionResult<IEnumerable<TEntity>>> GetByFilmId(string filmId);

        Task<ActionResult<IEnumerable<TEntity>>>GetByUtilisateurId(int utilisateurId);
        Task<ActionResult<TEntity>>GetByFilmUtilisateurId(string filmId, int utilisateurId);

        Task Add(TEntity entity);
        Task Update(TEntity entityToUpdate, TEntity entity);
        Task Delete(TEntity entity);
    }
}
