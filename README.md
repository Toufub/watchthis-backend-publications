# WatchThis - Backend Publications

## Fonctionnement API Publications

### FindAll - Get
Necessite un JWT Token
Code de retour :

* 200 : OK
* 401 : Unauthorized

/api/publication/
Structure d'un film :

````
{
    "filmId": "tt00001",
    "utilisateurId": 11,
    "note": 8,
    "commentaire": "ceci est un commentaire",
    "datePublication": "2023-01-15T00:00:00"
}
````


### ByUtilisateurId - GET
Necessite un JWT token
Code de retour :

* 200 : OK
* 401 : Unauthorized

/api/publications/byutilisateur/{id}
Renvoie une liste de publications

### ByFilmId - GET
Necessite un JWT token
Code de retour :

* 200 : OK
* 401 : Unauthorized

/api/publications/byfilm/{id}
Renvoie une liste de publications

### ByFilmUtilisateur - GET
Necessite un JWT token
Code de retour :

* 200 : OK
* 404 : Not found
* 401 : Unauthorized

/api/publications/byfilmutilisateur/{filmId}/{utilisateurId}
Renvoie une seule publication

### New - POST
Necessite un JWT Token
Code de retour :
* 201 : Created
* 401 : Unauthorized

/api/publications/

Structure à envoyer :
````
{
    "filmId": "tt00002",
    "note": "8",
    "commentaire": "ceci est un commentaire"
}
````

Renvoie la publication créée


### Edit - PUT
Necessite un JWT token
Code de retour :

* 204 : No Content
* 404 : Not found
* 401 : Unauthorized

````
{
    "note": "8",
    "commentaire": "ceci est un commentaire"
}
````

/api/publications/{filmId}

### Delete - DELETE
Necessite un JWT token
Code de retour :

* 204 : No Content
* 404 : Not found
* 401 : Unauthorized


/api/publications/{filmId}/{utilisateurId}